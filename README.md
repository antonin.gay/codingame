# Codingame challenges - C++

_A project by Antonin Gay_


## Description

[Codingame](https://www.codingame.com/) is a website proposing programming challenges to improve coding skills and prepare for tech interviews. As a beginning for my upcoming work search, I started completing those challenges.

This projects contains all my code for completing codingame challenges.


## Support

If you're passing by and want to give me advices on the code, I'll take them with great pleasure :D ! I'm eager to improve and any help or advice on the code, its optimization, its organization or the way to code would be great!
